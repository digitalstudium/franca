# franca

Multilingual static site generator based on Jinja templates.

Example theme: https://git.digitalstudium.com/digitalstudium/franca-theme-blog

Example website: https://git.digitalstudium.com/digitalstudium/digitalstudium.com (https://digitalstudium.com)

## Installation (Linux): 
```
curl https://git.digitalstudium.com/attachments/ceaf659d-f161-4ca4-86f9-172993b35e7e -o franca && chmod +x franca && sudo mv franca /usr/local/bin
```
## Usage:
Development: 
```
franca
```
Generate production-ready public folder:
```
franca -p
```